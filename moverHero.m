    %% This function creates a specified moving character in order to test for collision detection in our simulation
function moveHero( )
doCameraSpin = false;
[f,v,data] = plyread('arrow1.ply','tri');

% Get vertex count
HeroVertexCount = size(v,1);

% Move center point to origin
midPoint = sum(v)/HeroVertexCount;
HeroVerts = v - repmat(midPoint,HeroVertexCount,1);

% Create a transform to describe the location (at the origin, since it's centered
HeroPose = transl(1.4,1.6,0.2);

% Scale the colours to be 0-to-1 (they are originally 0-to-255
vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue] / 255;

% Then plot the trisurf
HeroMesh_h = trisurf(f,HeroVerts(:,1),HeroVerts(:,2), HeroVerts(:,3) ...
    ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');


%% To spin the camera 
if doCameraSpin
    for ax = -40:5:40; %#ok<UNRCH>
        for by = [[30:-3:0],[0:3:30]];
            view(ax,by);
            drawnow();
            pause(0.01);
        end;
    end
end

%% Then move the object once using Robot Toolbox transforms and without replot
% axis([-1 2 -1 2 -0.5 0.5]);

% Move forwards (facing in -y direction)
forwardTR = makehgtform('translate',[0,-0.01,0]);

% % Random rotate about Z
 randRotateTR = makehgtform('zrotate',0.5 * pi/180);

% Move the pose forward and a slight and random rotation
HeroPose = HeroPose * forwardTR * randRotateTR;
updatedPoints = [HeroPose * [HeroVerts,ones(HeroVertexCount,1)]']';  

% Now update the Vertices
HeroMesh_h.Vertices = updatedPoints(:,1:3);

%% Now move it many times
for i = 1:50
    % Random rotate about Z
    randRotateTR = makehgtform('zrotate',(-0.5) * pi/180);

    % Move forward then random rotation
    HeroPose = HeroPose * forwardTR * randRotateTR;

    % Transform the vertices
    
updatedPoints = [HeroPose * [HeroVerts,ones(HeroVertexCount,1)]']';
    
    % Update the mesh vertices in the patch handle
    HeroMesh_h.Vertices = updatedPoints(:,1:3);
    drawnow();
     
end
try delete (HeroMesh_h);
end; 
end

 