% Assignment 2 - ေူငေal11434697 and 11910128afa 

% Home Sorting Robot Source Code
function [] = Dobot( )adfdvd

clc;

% Setting up the joint angle limits, for joint limits 0>q>90 suggested joint limits are used.


    L1 = Link('d',0.084,'a',0,'alpha',-pi/2,'qlim',[deg2rad(-135),deg2rad(135)]);

    L2 = Link('d',0,'a',0.136,'alpha',0,'offset',-pi/2 ,'qlim',[deg2rad(5),deg2rad(80)]);

    L3 = Link('d',0,'a',0.15,'alpha',0,'qlim',[deg2rad(-5),deg2rad(85)]);

    L4 = Link('d',0,'a',0.04,'alpha',pi/2,'qlim',[deg2rad(-90),deg2rad(90)]);

    L5 = Link('d',0.06,'a',0,'alpha',0,'offset',0,'qlim',[deg2rad(-85),deg2rad(85)]);

DoBot = SerialLink([L1 L2 L3 L4 L5], 'name', 'DoBot');
q0 = [ deg2rad(75) deg2rad(65) deg2rad(35) deg2rad(-10) 0];
workspace = [-1 2 -1 2 -0.48 0.5];
scale = 0.1;
T=transl(0.1,0.4,0.4)*trotx(deg2rad(60))*troty(deg2rad(30));
q = DoBot.ikcon (T,q0);
DoBot.base = transl(0,0,-0.05)


% PlotAndColourRobot
for linkIndex = 0:DoBot.n
        [ faceData, vertexData, plyData{linkIndex+1} ] = plyread(['j',num2str(linkIndex),'.ply'],'tri'); 
       DoBot.faces{linkIndex+1} = faceData;
        DoBot.points{linkIndex+1} = vertexData;
end

% Display the robot
DoBot.plot3d(q0, 'workspace', workspace, 'scale', scale);

axis equal;
grid on;
camlight;
hold on
drawnow();    

% Define the Objects
rcube = transl(0.0805,0.3,0);
gcube = transl(0.1805,0.3,0);
bcube = transl(0.2805,0.3,0);
rbasket = transl(0,-0.28,-0.17);
gbasket = transl(0.2,-0.28,-0.17);
bbasket = transl(-0.2,-0.28,-0.17);

% Place the Extra-Objects for surrounding
makeObject('cabinet2.ply',0,0.05,-0.25,0,0,0)
makeObject('blind.ply',0.5,0.7,0.15,0,0,-90)
makeObject('cabinet3.ply',0,1.8,-0.17,0,0,0)
makeObject('cabinet3.ply',2.5,1,-0.17,0,0,90)
makeObject('smallcabinet.ply',0.0805,-0.15,-0.30,0,0,0)
makeObject('table.ply',0.0805,0.4,-0.06,0,0,0)
makeObject('button.ply',2.5,2,0,0,0,0)

% Model the Objects
[f,v,data] = plyread('rbasket.ply','tri');                                                % Red Basket                                         
objVertexCount1 = size(v,1);
midPoint1 = sum(v)/objVertexCount1;
objVerts1 = v - repmat(midPoint1,objVertexCount1,1);
objPose1 = rbasket;
vertexColours1 = [data.vertex.red, data.vertex.green, data.vertex.blue] / 255;
objMesh_h1 = trisurf(f,objVerts1(:,1),objVerts1(:,2), objVerts1(:,3) ...
    ,'FaceVertexCData',vertexColours1,'EdgeColor','interp','EdgeLighting','flat');
updatedPoints1 = [objPose1 * [objVerts1,ones(objVertexCount1,1)]']';  
 objMesh_h1.Vertices = updatedPoints1(:,1:3);
drawnow();


[f,v,data] = plyread('gbasket.ply','tri');                                                % Green Basket
objVertexCount2 = size(v,1);
midPoint2 = sum(v)/objVertexCount2;
objVerts2 = v - repmat(midPoint2,objVertexCount2,1);
objPose2 = gbasket;
vertexColours2 = [data.vertex.red, data.vertex.green, data.vertex.blue] / 255;
objMesh_h2 = trisurf(f,objVerts2(:,1),objVerts2(:,2), objVerts2(:,3) ...
    ,'FaceVertexCData',vertexColours2,'EdgeColor','interp','EdgeLighting','flat');
updatedPoints2 = [objPose2 * [objVerts2,ones(objVertexCount2,1)]']';  
 objMesh_h2.Vertices = updatedPoints2(:,1:3);
drawnow();


[f,v,data] = plyread('bbasket.ply','tri');                                                % Blue Basket
objVertexCount3 = size(v,1);
midPoint3 = sum(v)/objVertexCount3;
objVerts3 = v - repmat(midPoint3,objVertexCount3,1);
objPose3 = bbasket;
vertexColours3 = [data.vertex.red, data.vertex.green, data.vertex.blue] / 255;
objMesh_h3 = trisurf(f,objVerts3(:,1),objVerts3(:,2), objVerts3(:,3) ...
    ,'FaceVertexCData',vertexColours3,'EdgeColor','interp','EdgeLighting','flat');
updatedPoints3 = [objPose3 * [objVerts3,ones(objVertexCount3,1)]']';  
 objMesh_h3.Vertices = updatedPoints3(:,1:3);
drawnow();


[f,v,data] = plyread('rcube.ply','tri');                                                  % Red Cube
objVertexCount4 = size(v,1);
midPoint4 = (sum(v)/objVertexCount4)
objVerts4 = v - repmat(midPoint4,objVertexCount4,1)
objPose4 = rcube;
vertexColours4 = [data.vertex.red, data.vertex.green, data.vertex.blue] / 255;
objMesh_h4 = trisurf(f,objVerts4(:,1),objVerts4(:,2), objVerts4(:,3) ...
    ,'FaceVertexCData',vertexColours4,'EdgeColor','interp','EdgeLighting','flat');
updatedPoints4 = [objPose4 * [objVerts4,ones(objVertexCount4,1)]']';  
 objMesh_h4.Vertices = updatedPoints4(:,1:3);
drawnow();


[f,v,data] = plyread('gcube.ply','tri');                                                  % Green Cube
objVertexCount5 = size(v,1);
midPoint5 = sum(v)/objVertexCount5;
objVerts5 = v - repmat(midPoint5,objVertexCount5,1);
objPose5 = gcube;
vertexColours5 = [data.vertex.red, data.vertex.green, data.vertex.blue] / 255;
objMesh_h5 = trisurf(f,objVerts5(:,1),objVerts5(:,2), objVerts5(:,3) ...
    ,'FaceVertexCData',vertexColours5,'EdgeColor','interp','EdgeLighting','flat');
 updatedPoints5 = [objPose5 * [objVerts5,ones(objVertexCount5,1)]']';  
 objMesh_h5.Vertices = updatedPoints5(:,1:3);
drawnow();


[f,v,data] = plyread('bcube.ply','tri');                                                  % Blue Cube
objVertexCount6 = size(v,1);
midPoint6 = sum(v)/objVertexCount6;
objVerts6 = v - repmat(midPoint6,objVertexCount6,1);
objPose6 = bcube;
vertexColours6 = [data.vertex.red, data.vertex.green, data.vertex.blue] / 255;
objMesh_h6 = trisurf(f,objVerts6(:,1),objVerts6(:,2), objVerts6(:,3) ...
    ,'FaceVertexCData',vertexColours6,'EdgeColor','interp','EdgeLighting','flat');
updatedPoints6 = [objPose6 * [objVerts6,ones(objVertexCount6,1)]']'; 
objMesh_h6.Vertices = updatedPoints6(:,1:3);
drawnow();

camlight;
drawnow()

% Function for Moving the Robot 
q1 = DoBot.getpos
steps = 50;
function a = DoBotMove(transform, element)
        TRFORM = transform
        Toff=transl(0,0,-0.15);
        TRFROM = TRFORM(1:3,4) - Toff(1:3,4);
        TRFORM = transl(TR);
        mask = [1,1,1,0,0,0];
        TRFOMR1 = DoBot.getpos();
        qgoal = DoBot.ikcon(TR);
        qMatrix = jtraj(TRFORM1,qgoal,steps);
        s = lspb(0,1,steps);                                             	
        qMatrix = nan(steps,5);                                             
            for i = 1:steps
                collision()
                qMatrix(i,:) = (1-s(i))*q1 + s(i)*qgoal; 
                collision()% Generate interpolated joint angles
            end
        
velocity = zeros(steps,5);
acceleration  = zeros(steps,5);
for i = 2:steps
    collision()
    velocity(i,:) = qMatrix(i,:) - qMatrix(i-1,:);                        
    acceleration(i,:) = velocity(i,:) - velocity(i-1,:);
    collision()                                                        
end

        for i=1:steps
            collision()
            for j=1:3
                if qMatrix(i,j) < DoBot.qlim(j,1);
                    qMatrix(i,j) = DoBot.qlim(j,1);
                elseif qMatrix(i,j) > DoBot.qlim(j,2);
                    qMatrix(i,j) = DoBot.qlim(j,2);
                end
            end
            
            qMatrix(i,4) = (pi/2 - qMatrix(i,3) - qMatrix(i,2));
            collision()
        end
        if element == 0
            DoBot.animate(qMatrix);
            result = collision()
            drawnow();
        else
            for i=1:steps
                collision()
                qMatrix(i,:);
               

                DoBot.animate(qMatrix(i,:));
                result = collision()

                eTR = DoBot.fkine(qMatrix(i,:));
               collision()
                
                if element == 1
                    updatedPoints4 = [eTRFORM * [objVerts4,ones(objVertexCount4,1)]']';
                    objMesh_h4.Vertices = updatedPoints4(:,1:3)-[0,0,0.15];
                elseif element == 2
                    updatedPoints5 = [eTRFROM * [objVerts5,ones(objVertexCount5,1)]']';
                    objMesh_h5.Vertices = updatedPoints5(:,1:3)-[0,0,0.15];
                elseif element == 3
                    updatedPoints6 = [eTRFORM * [objVerts6,ones(objVertexCount6,1)]']';
                    objMesh_h6.Vertices = updatedPoints6(:,1:3)-[0,0,0.15];
                end
                if ~mod(steps,5)
                    drawnow();
                end
            end
        end
        a = 1;
        hold
end

% Return the collision value
result = collision()

% Run the simulation (if there is no collision)
while result == 0
result = collision()
    
TRFORM = rcube;
a = DoBotMove(TRFORM, 0)
TRFORM = rbasket;
a = DoBotMove(TRFORM, 1)
TRFORM = gcube;
 a = DoBotMove(TRFORM, 0)
TRFORM = gbasket
a = DoBotMove(TRFORM,2)

moveHero()

TRFORM =bcube;
a = DoBotMove(TRFORM,0)
TRFORM = bbasket
a = DoBotMove(TRFORM,3)
end

end

