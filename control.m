function control(robot, varargin)

    %parameters for the panel
    bgcol = [240 240 240]/255;                                             % background color
    height = 0.06;                                                         % height of slider rows
    
    %options
    opt.deg = true;
    opt.orientation = {'rpy', 'eul', 'approach'};
    opt.callback = [];    
    [opt,args] = tb_optparse(opt, varargin);
    handles.orientation = opt.orientation;
    handles.callback = opt.callback;
    handles.opt = opt;
    
    %limits of q
    qlim = robot.qlim;
    if any(isinf(qlim))
        error('Error!');
    end
    
    if isempty(args)
        q = [];
    else
        q = args{1};
    end
    
    %unit convertion 
    qscale = ones(robot.n,1);
    for j=1:robot.n
        L=robot.links(j);
        if opt.deg && L.isrevolute
            qscale(j) = 180/pi;
        end
    end
    
    handles.qscale = qscale;
    handles.robot = robot;
    
    %put the figure
    c = findobj(gca, 'Tag', robot.name);                                   % check the current axes
    if isempty(c)
        %find more if there is no figure in th current axes
        c = findobj(0, 'Tag', robot.name);                                 % check all figures
        if isempty(c)
            %create robot
            robot.plot( zeros(1, robot.n) );
            ax = gca;
        else
            ax = get(c(1), 'Parent');                                      % get first axis holding the robot
        end
    else
        %if there is figure in current axes
        ax = gca;
    end
    handles.fig = get(ax, 'Parent');                                       % get the figure that holds the axis
    
    set(ax, 'OuterPosition', [0.25 0 0.70 1])                              % shrink the current axes to make room [l b w h]
    handles.curax = ax;
    
    % create the panel
    panel = uipanel(handles.fig, ...
        'Title', 'Joint Angles', ...
        'BackGroundColor', bgcol,...
        'Position', [0 0.2 .3 0.5]);
    set(panel, 'Units', 'pixels');                                         % turn off automatic resizing
    handles.panel = panel;
    set(handles.fig, 'Units', 'pixels');

    set(handles.fig, 'ResizeFcn', @(src,event) resize_callback(robot, handles));
       
    %get the current robot state
    if isempty(q)
        %check for the robot
        rhandles = findobj('Tag', robot.name);        
        %find the graphical element of this name
        if isempty(rhandles)
            error('Error');
        end
        %get the info
        info = get(rhandles(1), 'UserData');       
        %the handle contains current joint angles
        if ~isempty(info.q)
            q = info.q;
        end
    else
    robot.plot(q);
    end
    handles.q = q;
    T6 = robot.fkine(q);
    
    %make the sliders
    n = robot.n;
    for j=1:n
        %slider label options
        uicontrol(panel, 'Style', 'text', ...
            'Units', 'normalized', ...
            'BackgroundColor', bgcol, ...
            'Position', [0 height*(n-j+2) 0.15 height], ...
            'FontUnits', 'normalized', ...
            'FontSize', 0.5, ...
            'String', sprintf('q%d', j));
        
        %slider options
        q(j) = max( qlim(j,1), min( qlim(j,2), q(j) ) ); 
        handles.slider(j) = uicontrol(panel, 'Style', 'slider', ...
            'Units', 'normalized', ...
            'Position', [0.15 height*(n-j+2) 0.65 height], ...
            'Min', qlim(j,1), ...
            'Max', qlim(j,2), ...
            'Value', q(j), ...
            'Tag', sprintf('Slider%d', j));
        
        %text box showing slider value
        handles.edit(j) = uicontrol(panel, 'Style', 'edit', ...
            'Units', 'normalized', ...
            'Position', [0.80 height*(n-j+2)+.01 0.20 0.9*height], ...
            'BackgroundColor', bgcol, ...
            'String', num2str(qscale(j)*q(j), 3), ...
            'HorizontalAlignment', 'left', ...
            'FontUnits', 'normalized', ...
            'FontSize', 0.4, ...
            'Tag', sprintf('Edit%d', j));
    end
    
    %set up the position display box   
    handles.t6.t(1) = uicontrol(panel, 'Style', 'text', ...
        'Units', 'normalized', ...
        'Position', [0 1-height 0.1 height], ...
        'FontUnits', 'normalized', ...
        'FontSize', 0.001, ...
        'String', sprintf('%.3f', T6(1,4)), ...
        'Tag', 'T6');
    
    handles.t6.t(2) = uicontrol(panel, 'Style', 'text', ...
        'Units', 'normalized', ...
        'Position', [0 1-height 0.1 height], ...
        'FontUnits', 'normalized', ...
        'FontSize', 0.001, ...
        'String', sprintf('%.3f', T6(2,4)));
    
    handles.t6.t(3) = uicontrol(panel, 'Style', 'text', ...
        'Units', 'normalized', ...
        'Position', [0 1-height 0.1 height], ...
        'FontUnits', 'normalized', ...
        'FontSize', 0.001, ...
        'String', sprintf('%.3f', T6(3,4)));
    
    %Orientation
    switch opt.orientation
        case 'approach'
            labels = {'ax:', 'ay:', 'az:'};
        case 'eul'
            labels = {[char(hex2dec('3c6')) ':'], [char(hex2dec('3b8')) ':'], [char(hex2dec('3c8')) ':']}; % phi theta psi
        case'rpy'
            labels = {'R:', 'P:', 'Y:'};
    end
    
    handles.t6.r(1) = uicontrol(panel, 'Style', 'text', ...
        'Units', 'normalized', ...
        'Position', [0.3 1-5*height 0.6 height], ...
        'FontUnits', 'normalized', ...
        'FontSize', 0.001, ...
        'String', sprintf('%.3f', T6(1,3)));
    
    handles.t6.r(2) = uicontrol(panel, 'Style', 'text', ...
        'Units', 'normalized', ...
        'Position', [0.3 1-6*height 0.6 height], ...
        'FontUnits', 'normalized', ...
        'FontSize', 0.001, ...
        'String', sprintf('%.3f', T6(2,3)));
    
    handles.t6.r(3) = uicontrol(panel, 'Style', 'text', ...
        'Units', 'normalized', ...
        'Position', [0.3 1-7*height 0.6 height], ...
        'FontUnits', 'normalized', ...
        'FontSize', 0.001, ...
        'String', sprintf('%.3f', T6(3,3)));   
    
    %callbacks
    for j=1:n
        %text edit box
        set(handles.edit(j), ...
            'Interruptible', 'off', ...
            'Callback', @(src,event)teach_callback(src, robot.name, j, handles));
        
        %slider
        set(handles.slider(j), ...
            'Interruptible', 'off', ...
            'BusyAction', 'queue', ...
            'Callback', @(src,event)teach_callback(src, robot.name, j, handles));
    end
end

function teach_callback(src, name, j, handles)
    
    qscale = handles.qscale;
    
    switch get(src, 'Style')
        case 'slider'
            %slider changed, get value and put it to edit box
            newval = get(src, 'Value');
            set(handles.edit(j), 'String', num2str(qscale(j)*newval, 3));
        case 'edit'
            %edit box changed, get value and put it to slider
            newval = str2double(get(src, 'String')) / qscale(j);
            set(handles.slider(j), 'Value', newval);
    end
    
    h = findobj('Tag', name);
    
    %look for the graphical element
    if isempty(h)
        error('Error!');
    end
    %get the info
    info = get(h(1), 'UserData');
    
    %update the joint coordinates
    info.q(j) = newval;
    %save it back to the robot
    set(h(1), 'UserData', info);
    
    %update all robots of this name
    animate(handles.robot, info.q);
        
    %compute the robot tool pose
    T6 = handles.robot.fkine(info.q);
    
    %convert orientation to desired format
    switch handles.orientation
        case 'approach'
            orient = T6(:,3);    % approach vector
        case 'eul'
            orient = tr2eul(T6, 'setopt', handles.opt);
        case'rpy'
            orient = tr2rpy(T6, 'setopt', handles.opt);
    end
    
    %update the display in the window
    for i=1:3
        set(handles.t6.t(i), 'String', sprintf('%.3f', T6(i,4)));
        set(handles.t6.r(i), 'String', sprintf('%.3f', orient(i)));
    end
    
    if ~isempty(handles.callback)
        handles.callback(handles.robot, info.q);
    end

end

function record_callback(robot, handles)
    
    if ~isempty(handles.callback)
        handles.callback(h.q);
    end
end


function resize_callback(robot, handles)

    %resize figure
    fig = gcbo;                                                             
    fs = get(fig, 'Position');                                             % get size of figure
    ps = get(handles.panel, 'Position');                                   % get position of the panel
    %update dimensions of the axis area
    set(handles.curax, 'Units', 'pixels', ...
        'OuterPosition', [ps(3) 0 fs(3)-ps(3) fs(4)]);
    %keep the panel anchored to the top left corner
    set(handles.panel, 'Position', [1 fs(4)-ps(4) ps(3:4)]);
end
