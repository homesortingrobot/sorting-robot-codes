
close all 
clear all

%% This function creates, scale colors and plots objects with input translational parameters (x1,y1,z1) and rotational parameters of (rx, ry, rz) and returns two outputs y and z.


function [y,z] = makeObject( name, x1, y1, z1,rx,ry,rz)

[f,v,data] = plyread(name,'tri');

% Get vertex count
objVertexCount = size(v,1);

% Move center point to origin
midPoint = sum(v)/objVertexCount;
objVerts = v - repmat(midPoint,objVertexCount,1);

% Create a transform to describe the location (at the origin, since it's centered
objPose = eye(4);

% Scale the colours to be 0-to-1 (they are originally 0-to-255
vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue] / 255;

% Then plot the trisurf
objMesh_h = trisurf(f,objVerts(:,1),objVerts(:,2), objVerts(:,3) ...
    ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');

% 
% Move forwards (facing in -y direction)
  forwardTR = makehgtform('translate',[x1,y1,z1]);

% Random rotate about Z
 randRotateTR = makehgtform('xrotate',deg2rad(rx),'yrotate',deg2rad(ry),'zrotate',deg2rad(rz));
 
% Move the pose forward and a slight and random rotation
 objPose = objPose * forwardTR * randRotateTR
 updatedPoints = [objPose * [objVerts,ones(objVertexCount,1)]']';  

% Now update the Vertices
 objMesh_h.Vertices = updatedPoints(:,1:3);
x = objMesh_h.Vertices;
y = objPose;
z = updatedPoints;

end
