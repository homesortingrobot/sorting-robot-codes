%% Collision
function result = collision(DoBot,qMatrix,faces,vertex,faceNormals,returnOnceFound)
if nargin < 6
    returnOnceFound = false;
else
    returnOnceFound = true;

for qIndex = 1:size(qMatrix,1)
    q = q1(qMatrix,:);

    % Update the joints
    tr = zeros(4,4,DoBot.n+1);
    tr(:,:,1) = DoBot.base;
    L = DoBot.links;
    for i = 1 : DoBot.n
        tr(:,:,i+1) = tr(:,:,i) * trotz(q(i)) * transl(0,0,L(i).d) * transl(L(i).a,0,0) * trotz(L(i).alpha);
    end

    % Check the links and the faces
    for i = 1 : size(tr,3)-1    
        for faceIndex = 1:size(faces,1)
            vertOnPlane = vertex(faces(faceIndex,1)',:);
            [intersectP,check] = LinePlaneIntersection(faceNormals(faceIndex,:),vertOnPlane,tr(1:3,4,i)',tr(1:3,4,i+1)'); 
            if check == 1 && IsIntersectionPointInsideTriangle(intersectP,vertex(faces(faceIndex,:)',:))
                plot3(intersectP(1),intersectP(2),intersectP(3),'g*');
                display('Intersection');
                result = true;
                if returnOnceFound
                    pause
                    return
                end
            end
        end    
    end
end
end
result = returnOnceFound
end