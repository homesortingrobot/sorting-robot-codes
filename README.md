# README #

Our team will be researching different applications of small robot system at home, the robot will be picking up different coloured acrylic plates and sorting it in different compartments in a box. For this purpose, we will be using the Dobot Robotic Arm with suction cap gripper. We might also trial another application � picking up and sorting different coloured socks (a pincer gripper will be utilised), after the completion of a successful simulation of the previous study.  Our model will have collision detection also colour, depth and workspace sensing for safety.

For the different coloured acrylic plates project, we will be using the suction cap gripper as all the plates have the same height i.e., the same z value. For the sorting different coloured socks, the pincer gripper is used because the socks are made of fabric and having non-uniform shapes.

### What is this repository for? ###

* Home Based Sorting Robot
* Version 0.0
