function varargout = DobotGUI(varargin)
% DOBOTGUI MATLAB code for DobotGUI.fig
%      DOBOTGUI, by itself, creates a new DOBOTGUI or raises the existing
%      singleton*.
%
%      H = DOBOTGUI returns the handle to a new DOBOTGUI or the handle to
%      the existing singleton*.
%
%      DOBOTGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DOBOTGUI.M with the given input arguments.
%
%      DOBOTGUI('Property','Value',...) creates a new DOBOTGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before DobotGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to DobotGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help DobotGUI

% Last Modified by GUIDE v2.5 15-Oct-2017 22:42:06

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @DobotGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @DobotGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before DobotGUI is made visible.
function DobotGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to DobotGUI (see VARARGIN)

% Choose default command line output for DobotGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes DobotGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);
    L1 = Link('d',0.084,'a',0,'alpha',-pi/2,'qlim',[deg2rad(-135),deg2rad(+135)]);

    L2 = Link('d',0,'a',0.136,'alpha',0,'offset',-pi/2 ,'qlim',[deg2rad(5),deg2rad(80)]);

    L3 = Link('d',0,'a',0.15,'alpha',0,'qlim',[deg2rad(-5),deg2rad(85)]);

    L4 = Link('d',0,'a',0.04,'alpha',pi/2,'qlim',[deg2rad(-90),deg2rad(90)]);

    L5 = Link('d',0.06,'a',0,'alpha',0,'offset',0,'qlim',[deg2rad(-85),deg2rad(85)]);

DoBot = SerialLink([L1 L2 L3 L4 L5], 'name', 'DoBot');

DoBot.base = transl(0,0,-0.05)
q0 = [ deg2rad(75) deg2rad(65) deg2rad(35) deg2rad(-10) 0];
workspace = [-1 2 -1 2 -0.48 0.5];
scale = 0.1;
T=transl(0.1,0.4,0.4)*trotx(deg2rad(60))*troty(deg2rad(30));
q = DoBot.ikcon (T,q0)
for linkIndex = 0:DoBot.n
        [ faceData, vertexData, plyData{linkIndex+1} ] = plyread(['j',num2str(linkIndex),'.ply'],'tri'); 
       DoBot.faces{linkIndex+1} = faceData;
        DoBot.points{linkIndex+1} = vertexData;
end
 DoBot.plot3d(q, 'workspace', workspace, 'scale', scale);
axis equal;
grid on;
camlight;
%% PlotAndColourRobot
% Given a robot index, add the glyphs (vertices and faces) and
% colour them in if data is available 
for linkIndex = 0:DoBot.n
        [ faceData, vertexData, plyData{linkIndex+1} ] = plyread(['j',num2str(linkIndex),'.ply'],'tri'); 
       DoBot.faces{linkIndex+1} = faceData;
        DoBot.points{linkIndex+1} = vertexData;
end

axis equal;
grid on;
camlight;
hold on
drawnow();    

camlight;
DoBot.plot3d(q, 'workspace', workspace, 'scale', scale);
DoBot.teach


% --- Outputs from this function are returned to the command line.
function varargout = DobotGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in demoButn.
function demoButn_Callback(hObject, eventdata, handles)
% hObject    handle to demoButn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
source();

% --- Executes on button press in stopButn.
function stopButn_Callback(hObject, eventdata, handles)
% hObject    handle to stopButn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = msgbox('The programme is stopping now.', 'Warning!', 'warn');
uiwait();

% --- Executes on button press in resumeButn.
function resumeButn_Callback(hObject, eventdata, handles)
% hObject    handle to resumeButn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiresume();




function xSetTxt_Callback(hObject, eventdata, handles)
% hObject    handle to xSetTxt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of xSetTxt as text
%        str2double(get(hObject,'String')) returns contents of xSetTxt as a double



% --- Executes during object creation, after setting all properties.
function xSetTxt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xSetTxt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ySetTxt_Callback(hObject, eventdata, handles)
% hObject    handle to ySetTxt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ySetTxt as text
%        str2double(get(hObject,'String')) returns contents of ySetTxt as a double


% --- Executes during object creation, after setting all properties.
function ySetTxt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ySetTxt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function zSetTxt_Callback(hObject, eventdata, handles)
% hObject    handle to zSetTxt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of zSetTxt as text
%        str2double(get(hObject,'String')) returns contents of zSetTxt as a double


% --- Executes during object creation, after setting all properties.
function zSetTxt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to zSetTxt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in setendButn.
function setendButn_Callback(hObject, eventdata, handles)
% hObject    handle to setendButn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
x = str2double(get(handles.xSetTxt, 'string'));
y = str2double(get(handles.ySetTxt, 'string'));
z = str2double(get(handles.zSetTxt, 'string'));


    L1 = Link('d',0.084,'a',0,'alpha',-pi/2,'qlim',[deg2rad(-135),deg2rad(+135)]);

    L2 = Link('d',0,'a',0.136,'alpha',0,'offset',-pi/2 ,'qlim',[deg2rad(5),deg2rad(80)]);

    L3 = Link('d',0,'a',0.15,'alpha',0,'qlim',[deg2rad(-5),deg2rad(85)]);

    L4 = Link('d',0,'a',0.04,'alpha',pi/2,'qlim',[deg2rad(-90),deg2rad(90)]);

    L5 = Link('d',0.06,'a',0,'alpha',0,'offset',0,'qlim',[deg2rad(-85),deg2rad(85)]);

DoBot = SerialLink([L1 L2 L3 L4 L5], 'name', 'DoBot');

q0 = [ deg2rad(75) deg2rad(65) deg2rad(35) deg2rad(-10) 0];
workspace = [-1 2 -1 2 -0.48 0.5];
scale = 0.1;
T=transl(0.1,0.4,0.4)*trotx(deg2rad(60))*troty(deg2rad(30));

q = DoBot.ikcon (T,q0)
for linkIndex = 0:DoBot.n
        [ faceData, vertexData, plyData{linkIndex+1} ] = plyread(['j',num2str(linkIndex),'.ply'],'tri'); 
       DoBot.faces{linkIndex+1} = faceData;
        DoBot.points{linkIndex+1} = vertexData;
end

%scale the plot
 DoBot.plot3d(q, 'workspace', workspace, 'scale', scale);
axis equal;
grid on;
camlight;

Tr_init= DoBot.getpos()

Tr_move = transl (x,y,z);
Tr_goal = DoBot.ikcon(Tr_move)
animate = nan (1,50);
qMatrix = jtraj(Tr_init,Tr_goal,50);
DoBot.animate (qMatrix)
